LLVM Toolchain for Morello
==========================

This repository provides binaries for released versions of Morello LLVM toolchain.
Use the links or Git commands provided below to download toolchain for the desired
host platform and target system.

## Latest Release

Latest release for Linux is 1.9. Source: [morello/release-1.9][git-tag-linux].
Latest release for Baremetal is 1.8. Source: [morello/release-1.8][git-tag].
Latest release for Android is 1.6. Source: [morello/release-1.6][android].

| Host      | AArch64                                                | x86                                 				|
| :---      | :---                                                   | :---                                             |
| Android   | --                            						 | [morello-llvm-android-1.6.tar.gz][android]     	|
| Baremetal | --                                                     | [morello-llvm-baremetal-1.8.tar.gz][baremetal] 	|
| Linux     | [morello-llvm-linux-aarch64-1.9.tar.gz][linux-aarch64] | [morello-llvm-linux-1.9.tar.gz][linux] 			|

## Checkout using Git

Set Git URL:

```
repo=https://git.morello-project.org/morello/llvm-project-releases.git
```

Choose branch:

```
branch=morello/android-release-1.6        # for Android target and x86 host
branch=morello/baremetal-release-1.8      # for Baremetal target and x86 host
branch=morello/linux-release-1.9          # for Linux target and x86 host
branch=morello/linux-aarch64-release-1.9  # for Linux target and AArch64 host
```

Checkout binaries:

```
git init
git fetch -- ${repo} +refs/heads/${branch}:refs/remotes/origin/${branch}
git checkout origin/${branch} -b ${branch}
```

[git-tag]: https://git.morello-project.org/morello/llvm-project/-/tree/morello/release-1.9
[git-tag-linux]: https://git.morello-project.org/morello/llvm-project/-/tree/morello/release-1.9
[android]: https://git.morello-project.org/morello/llvm-project-releases/-/archive/morello/android-release-1.6/llvm-project-releases-morello-android-release-1.6.tar.gz
[baremetal]: https://git.morello-project.org/morello/llvm-project-releases/-/archive/morello/baremetal-release-1.8/llvm-project-releases-morello-baremetal-release-1.8.tar.gz
[linux-aarch64]: https://git.morello-project.org/morello/llvm-project-releases/-/archive/morello/linux-aarch64-release-1.9/llvm-project-releases-morello-linux-aarch64-release-1.9.tar.gz
[linux]: https://git.morello-project.org/morello/llvm-project-releases/-/archive/morello/linux-release-1.9/llvm-project-releases-morello-linux-release-1.9.tar.gz
